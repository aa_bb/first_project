import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

public class HW {
    public static void main(String[] args) {
        String strJson = "{\"int\" : 41, \"str\" : \"privet\",\"array\" : [1, 2, 3, 4, 5]}";
        String strObj = "{\"float\" : 3.14, \"str2\" : \"poka\"}";

        JSONObject result = (JSONObject) JSONValue.parse(strJson);
        JSONObject obj = (JSONObject) JSONValue.parse(strObj);

        result.put("obj", obj);

        System.out.println(result.toString());
    }
}

