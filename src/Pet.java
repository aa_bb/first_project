class Pet {
    protected String name;
    protected int age;
    protected int legs;

    Pet(String name, int age, int legs) {
        this.name = name;
        this.age = age;
        this.legs = legs;
    }

    @Override
    public String toString() {
        return "Объект базового класса: " + name + " " + age + " " + legs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLegs() {
        return legs;
    }

    public void setLegs(int legs) {
        this.legs = legs;
    }
}
