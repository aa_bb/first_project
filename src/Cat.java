public class Cat extends Pet {
    private int tailLength;

    Cat(String name, int age,  int legs, int tailLength) {
        super(name, age, 4);
        this.tailLength = tailLength;
    }

    @Override
    public String toString() {
        return "Объект производного класса: " + name + " " + age + " " + legs + " " + tailLength;
    }

    public int getTailLength() {
        return tailLength;
    }

    public void setTailLength(int tailLength) {
        this.tailLength = tailLength;
    }
}
